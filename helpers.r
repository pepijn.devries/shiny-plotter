library (lattice)

graphics.off()

xdummy <- rnorm(10)
ydummy <- rnorm(10)

dummyplot <- function() {
  print(xyplot(ydummy ~ xdummy, NULL, main = "test"))
}