#' previewOutput
#'
#' Setup a user interface to produce a preview in Shiny
#' 
#' Use in combination with \code{renderPreview} to create a low resolution preview
#' at the client side of the Shiny app. A high resolution can be obtained by opening
#' a graphics device with the specified target width, height and resolution.
#' @param id a \code{character} string representing the id of the preview image.
#' @param ... any aditional parameters send to \code{plotOutput}.
#' @return Returns anything returned by the \code{plotOutput} function.
#' There is no need to use the return value.
#' @author Pepijn de Vries
previewOutput <- function(id, ...) {
  ns <- NS(id)
  plotOutput(ns("out"), ...)
}

#' renderPreview
#'
#' Shiny module that handles the the rendering of a preview on the server.
#' 
#' Use in combination with \code{previewOutput} to create a low resolution preview
#' at the client side of the Shiny app. A high resolution can be obtained by opening
#' a graphics device with the specified target width, height and resolution.
#' @param id a \code{character} string representing the id of the preview image.
#' @param expr An expression that contains the plotting routines that should be
#' displayed in the preview
#' @param target_width The target width (in the unit specified below) for a high
#' resolution version of the preview.
#' @param target_height  The target height (in the unit specified below) for a high
#' resolution version of the preview.
#' @param target_res The desired resolution for the high resolution version of the
#' preview in dpi.
#' @param target_unit Unit used for the target dimensions. can be `px` (pixels), `cm`
#' or `in` (inches).
#' @param preview_width width of preview plot in pixels.
#' @param preview_height height of preview plot in pixels.
#' @param ... any aditional parameters used in \code{expr}
#' @return Returns anything returned by the \code{expr}.
#' There is no need to use the return value.
#' @author Pepijn de Vries
renderPreview <- function(id, expr, target_width, target_height, target_res, target_unit = c("px", "cm", "in"),
                          preview_width = 800, preview_height = 0, ...) {
  # make additional parameters available for expression
  for (obj in names(list(...))) {
    assign(obj, list(...)[[obj]])
  }
  moduleServer(
    id,
    function(input, output, session) {
      target_unit <- match.arg(target_unit, c("px", "cm", "in"))
      asprat  <- target_width/target_height
      if ((preview_width*asprat) > preview_height) {
        if (preview_height*asprat < preview_width && preview_height > 0) {
          preview_width <- preview_height*asprat
        } else {
          preview_height <- preview_width/asprat
        }
      }
      # convert target width and height to pixels:
      if (target_unit %in% c("cm", "in")) {
        target_width <- target_res*target_width / ifelse(target_unit == "cm", cm(1), 1)
        target_height <- target_res*target_height / ifelse(target_unit == "cm", cm(1), 1)
      }
      scale <- preview_height/target_height
      output$out <- renderPlot({
        tryCatch(eval(expr))
      },
      width  = preview_width,
      height = preview_height,
      res    = target_res*scale,
      type   = "cairo")
    })
}
