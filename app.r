library(shiny)
source("helpers.r")

ui <- fluidPage(
  fluidRow(
    shiny::column(3,
      selectInput("selectUnit", "Unit", c("cm", "in", "px"), "px"),
      numericInput("numericTargetWidth", "Target width", min = 0.5, value = 800),
      numericInput("numericTargetHeight", "Target height", min = 0.5, value = 600),
      numericInput("numericTargetRes", "Target resolution (dpi)", min = 0.5, value = 72),
      downloadButton("downloadImage", "Download plot")
    ),
    shiny::column(9,
      previewOutput("preview")
    )
  )
)

server <- function(input, output, session) {
  observe({
    w <- req(input$numericTargetWidth)
    h <- req(input$numericTargetHeight)
    u <- req(input$selectUnit)
    r <- req(input$numericTargetRes)
    renderPreview("preview", {dummyplot()}, w, h, r, u, pointsize = 20)
  })
  
  output$downloadImage <- downloadHandler(
    filename = function() { paste0("plot-", Sys.Date(), ".png") },
    content = function(con) {
      png(con, input$numericTargetWidth, input$numericTargetHeight, input$selectUnit, res = input$numericTargetRes)
      tryCatch(dummyplot())
      dev.off()
    }
  )
}

shinyApp(ui, server)